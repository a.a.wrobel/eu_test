/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blueqbit.sbjsf.model;
import java.sql.Timestamp;
import javax.persistence.Column;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;



/**
 *
 * @author aaw
 */
@Entity
@Data
public class Feedback {
    @Id
    @GeneratedValue
    private Long feedbackId;
    
    @Size(max=100)
    @Column(length = 100)
    private String name;
    
    @Size(max=100)
    @Column(length = 100)
    private String email;
    
   
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="contact_type_id")
    private ContactType  contactType;
    
    @Size(max=1000)
    @Column(length = 1000)
    private String message;
    
    private Timestamp submissionTimestamp;
    
   
    
}

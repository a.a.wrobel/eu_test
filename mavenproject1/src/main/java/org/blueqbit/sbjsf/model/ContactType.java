/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blueqbit.sbjsf.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;

/**
 *
 * @author aaw
 */
@Entity
@Data
public class ContactType implements Serializable {
    
 
    @Id
    @Column(name="contact_type_id")
    private Long contactTypeId;
    
    private String contactName;
    
    public ContactType() {
        
    }
    
    public ContactType(Long ctId, String ctName) {
        contactTypeId = ctId;
        contactName = ctName;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blueqbit.sbjsf.controller;

/**
 *
 * @author aaw
 */
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.blueqbit.sbjsf.model.ContactType;

import org.blueqbit.sbjsf.model.Feedback;
import org.blueqbit.sbjsf.repository.ContactTypeRepository;
import org.blueqbit.sbjsf.repository.FeedbackRepository;

@Named
@ViewScoped
public class FeedbackController {
    private Feedback feedback;
    private List<Feedback> allFeedbacks;
    private List<Feedback> filteredFeedbacks;
    
    private FeedbackRepository feedbackRepository;
    private ContactTypeRepository contactTypeRepository;
    
    
    public FeedbackController() {
        feedback = createEmptyFeedback();
        this.feedbackRepository = feedbackRepository;
        this.contactTypeRepository =  contactTypeRepository;
    }
    
    
    
  

    @Autowired
    public FeedbackController(FeedbackRepository feedbackRepository, ContactTypeRepository contactTypeRepository) {
        feedback = createEmptyFeedback();
        this.feedbackRepository = feedbackRepository;
        this.contactTypeRepository =  contactTypeRepository;
    }
    
    public void save() {
        FacesContext context = FacesContext.getCurrentInstance();
        ContactType ct = contactTypeRepository.findById(feedback.getContactType().getContactTypeId().longValue());
        feedback.setContactType(ct);
        feedback.setSubmissionTimestamp(Timestamp.from(Instant.now()));
        feedbackRepository.save(feedback);    
        feedback = createEmptyFeedback(); 
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,  "Success",  "Your feedback successfully stored") );
    }
    
    public Feedback getFeedback() {
        return feedback;
    }
    public void setFeedback(Feedback fb) {
        feedback = fb;
    }
    
    public List<Feedback> getAllFeedbacks() {
        fetchAll();
        return allFeedbacks;
    }
    public void setAllFeedback(List<Feedback> fbs) {
        allFeedbacks = fbs;
    }
    
    private Feedback createEmptyFeedback() {
        Feedback fb = new Feedback();
        fb.setContactType(new ContactType());
        return fb;
    }
    
    public void fetchAll() {
        allFeedbacks = feedbackRepository.findAll();
    }
    
    public List<Feedback> getFilteredFeedbacks() {
        return filteredFeedbacks;
    }
    public void setFilteredFeedbacks(List<Feedback> fbs) {
        filteredFeedbacks = fbs;
    }
    
}

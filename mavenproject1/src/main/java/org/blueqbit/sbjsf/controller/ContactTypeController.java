/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blueqbit.sbjsf.controller;

/**
 *
 * @author aaw
 */
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import org.blueqbit.sbjsf.model.ContactType;

import org.blueqbit.sbjsf.repository.ContactTypeRepository;


@Named
@ViewScoped
public class ContactTypeController {

    private List<ContactType> contactTypes = new ArrayList<>();
    private ContactTypeRepository contactTypeRepository;
    
    @Autowired
    public ContactTypeController(ContactTypeRepository ctp) {
        contactTypeRepository = ctp;
    }
    
    public  List<ContactType> fetchAll() {
        contactTypes = contactTypeRepository.findAll();
        if (contactTypes == null) {
            contactTypes = new ArrayList<>();
        }  
        return contactTypes;
    }
    
    public List<SelectItem> getAllContactTypes() {
        List<SelectItem> items = new ArrayList<>();
        fetchAll().forEach(ct -> {
            items.add(new SelectItem(ct.getContactTypeId(), ct.getContactName()));
        });
        return items;
    }
    
    public List<String> getContactTypeNames() {
        List<String> retVal = new ArrayList<>();
        fetchAll().forEach(ct -> {
            retVal.add(ct.getContactName());
        });
        return retVal;
    }
    
}

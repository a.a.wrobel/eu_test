/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blueqbit.sbjsf.repository;
import java.util.List;
import org.blueqbit.sbjsf.model.ContactType;



import org.springframework.data.repository.CrudRepository;
/**
 *
 * @author aaw
 */
public interface ContactTypeRepository extends CrudRepository<ContactType, Long> {
    
    ContactType findById(long id);
    
    @Override
    List<ContactType> findAll();
    
  
    
}

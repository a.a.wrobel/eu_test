/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blueqbit.sbjsf.controller;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import org.blueqbit.sbjsf.model.ContactType;
import org.blueqbit.sbjsf.repository.ContactTypeRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author aaw
 */
public class ContactTypeControllerTest {
    
    public ContactTypeControllerTest() {
    }
    
   

    /**
     * Test of fetchAll method, of class ContactTypeController.
     */
    @Test
    public void testFetchAll() {
        System.out.println("fetchAll");
        ContactTypeRepository cprMock = mock(ContactTypeRepository.class);
        ArrayList<ContactType> ctList = new ArrayList<>();
        ContactType ct = new ContactType(1L, "contactType");
        ctList.add(ct);
        
        when(cprMock.findAll()).thenReturn(ctList);
        ContactTypeController instance = new ContactTypeController(cprMock);
        
        List<ContactType> result = instance.fetchAll();
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(1L, result.get(0).getContactTypeId().longValue());
        Assertions.assertTrue("contactType".equals(result.get(0).getContactName()));
    }

    /**
     * Test of getAllContactTypes method, of class ContactTypeController.
     */
    @Test
    public void testGetAllContactTypes() {
        System.out.println("getAllContactTypes");
                ContactTypeRepository cprMock = mock(ContactTypeRepository.class);
        ArrayList<ContactType> ctList = new ArrayList<>();
        ContactType ct = new ContactType(1L, "contactType");
        ctList.add(ct);
        
        when(cprMock.findAll()).thenReturn(ctList);
        ContactTypeController instance = new ContactTypeController(cprMock);
        
        
        
        List<SelectItem> result = instance.getAllContactTypes();
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(1L, ((Long)result.get(0).getValue()).longValue());
        Assertions.assertTrue("contactType".equals(result.get(0).getLabel()));

    }

    /**
     * Test of getContactTypeNames method, of class ContactTypeController.
     */
    @Test
    public void testGetContactTypeNames() {
        System.out.println("getContactTypeNames");
        ContactTypeRepository cprMock = mock(ContactTypeRepository.class);
        ArrayList<ContactType> ctList = new ArrayList<>();
        ContactType ct = new ContactType(1L, "contactType");
        ctList.add(ct);
        
        when(cprMock.findAll()).thenReturn(ctList);
        ContactTypeController instance = new ContactTypeController(cprMock);
        
        List<String> result = instance.getContactTypeNames();        
        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue("contactType".equals(result.get(0)));
   
    }
    
}

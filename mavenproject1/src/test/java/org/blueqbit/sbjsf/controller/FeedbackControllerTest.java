/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.blueqbit.sbjsf.controller;

import java.util.ArrayList;
import java.util.List;

import org.blueqbit.sbjsf.model.ContactType;
import org.blueqbit.sbjsf.model.Feedback;
import org.blueqbit.sbjsf.repository.ContactTypeRepository;
import org.blueqbit.sbjsf.repository.FeedbackRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.atLeast;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author aaw
 */
public class FeedbackControllerTest {
    
    public FeedbackControllerTest() {
    }

    

    

    

    /**
     * Test of getAllFeedbacks method, of class FeedbackController.
     */
    @Test
    public void testGetAllFeedbacks() {
        System.out.println("getAllFeedbacks");
        
        FeedbackRepository feedbackRepository = mock(FeedbackRepository.class);
        ContactTypeRepository contactTypeRepository = mock(ContactTypeRepository.class);
        Feedback feedback = new Feedback();
        List<Feedback> feedbacks = new ArrayList<>();
        ContactType contactType = new ContactType(1L, "contactType");
        feedback.setContactType(contactType);
        when(feedbackRepository.findAll()).thenReturn(feedbacks);
        
        FeedbackController instance 
                = new FeedbackController( feedbackRepository, contactTypeRepository);
        
        List<Feedback> result = instance.getAllFeedbacks();
        verify(feedbackRepository, atLeast(1)).findAll();
    }

    

    /**
     * Test of fetchAll method, of class FeedbackController.
     */
    @Test
    public void testFetchAll() {
        System.out.println("fetchAll");
         FeedbackRepository feedbackRepository = mock(FeedbackRepository.class);
        ContactTypeRepository contactTypeRepository = mock(ContactTypeRepository.class);
        Feedback feedback = new Feedback();
        List<Feedback> feedbacks = new ArrayList<>();
        ContactType contactType = new ContactType(1L, "contactType");
        feedback.setContactType(contactType);
        when(feedbackRepository.findAll()).thenReturn(feedbacks);
        
        FeedbackController instance 
                = new FeedbackController( feedbackRepository, contactTypeRepository);
        instance.fetchAll();
        List<Feedback> result = instance.getAllFeedbacks();
        Assertions.assertEquals(result, feedbacks);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    
    
}
